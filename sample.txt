db.createUser({
    user: "admin",
    pwd: "secret123",
    roles: [{
        role: "readWrite",
        db: "Mojito"
    }]
})


=============== group-item ===============

{
    "_id" : ObjectId("group_item_01"),
    "object_type" : "text",
    "random" : true,
    "item_ids" : [ 
        ObjectId("item_text_01")
    ],
    "name" : "group_text_01"
}

=============== item ===============

1. text

{
    "_id" : ObjectId("item_text_01"),
    "item_type" : "text",
    "name" : "text_01",
    "payload" : {
        "text" : "สวัสดีค่ะ"
    }
}


2. generic

{
    "_id" : ObjectId("item_generic_01"),
    "item_type" : "item",
    "payload" : {
        "title" : "เมนูเริ่มต้น",
        "item_url" : "",
        "image_url" : "",
        "subtitle" : "",
        "buttons" : [ 
            {
                "type" : "postback",
                "payload" : "button_01",
                "title" : "ปุ่มที่1"
            }, 
            {
                "type" : "postback",
                "payload" : "button_02",
                "title" : "ปุ่มที่2"
            }, 
            {
                "type" : "postback",
                "payload" : "button_03",
                "title" : "ปุ่มที่3"
            }
        ]
    },
    "name" : "menu_generic",
}


3. img

{
    "_id" : ObjectId("item_img_01"),
    "item_type" : "item",
    "payload" : {
        "url" : ""
    },
    "name" : "image_01"
}


4. button

{
    "_id" : ObjectId("button_01"),
    "item_type" : "item",
    "payload" : {
        "text" : "คลิกเลย !",
        "buttons" : [ 
            {
                "type" : "postback",
                "payload" : "button_01",
                "title" : "ปุ่มที่1"
            }, 
            {
                "type" : "postback",
                "payload" : "button_02",
                "title" : "ปุ่มที่2"
            }
        ]
    },
    "name" : "menu_button_01"
}


5. quick_replies

{
    "_id" : ObjectId("quick_replies_01")
    "item_type" : "quick_replies",
    "payload" : {
        "content_type" : "text",
        "title" : "ชื่อปุ่ม",
        "payload" : "button_01"
    },
    "name" : "quick_replies_01"
}

