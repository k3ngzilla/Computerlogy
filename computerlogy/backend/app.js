const koa = require('koa')
const mongo = require('koa-mongo')
const Router = require('koa-router')
const bodyParser = require('koa-bodyparser')
const cors = require('koa2-cors')
const app = new koa()
const router = new Router()
const ObjectID = require('mongodb').ObjectID
const sanitize = require('mongo-sanitize')
app.use(
  mongo({
    host: '139.5.146.208',
    port: 27017,
    user: 'root',
    pass: 'p%40ssword123',
    db: 'Mojito',
    max: 100,
    min: 1
  })
)
router.get('/groupitemall', async function (ctx) {
  ctx.body = await ctx.mongo
    .db('Mojito')
    .collection('group_item')
    .find({}, { name: 1 })
    .toArray()
})
router.get('/groupitem/:id', async function (ctx) {
  let group = ctx.request.body
  ctx.body = await ctx.mongo
    .db('Mojito')
    .collection('group_item')
    .find({ _id: sanitize(ObjectID(ctx.params.id)) })
    .toArray()
})
router.post('/groupitem', async function (ctx) {
  let group = ctx.request.body

  ctx.body= await ctx.mongo.db('Mojito').collection('group_item').insert({
    object_type: sanitize(group.object_type),
    random: true,
    item_ids:[],
    name: sanitize(group.name)
  })
})
router.put('/groupitem', async function (ctx) {
  let group = ctx.request.body

  ctx.body = await ctx.mongo.db('Mojito').collection('group_item').update(
    { _id: sanitize(ObjectID(group._id)) },
    {
      $set: {
        item_ids: sanitize(group.item_ids)
      }
    }
  )
})
router.delete('/groupitem', async function (ctx) {
  let group = ctx.request.body
  await ctx.mongo
    .db('Mojito')
    .collection('group_item')
    .deleteOne({ _id: sanitize(ObjectID(group._id)) })
})
router.put('/item', async function (ctx) {
  let item = ctx.request.body

  ctx.body = await ctx.mongo.db('Mojito').collection('item').update(
    { _id: sanitize(ObjectID(item._id)) },
    {
      $set: {
        payload : {text:sanitize(item.payload.text)}
      }
    }
  )
})

router.get("/item/:id",async function (ctx) {
let selectGroupItem =  await ctx.mongo.db('Mojito').collection('group_item').find({"_id":sanitize(ObjectID(ctx.params.id))},{_id:0,item_ids:1}).toArray();
let itemId = selectGroupItem[0].item_ids;
let convertIdToObjectId = itemId.map(function (id) {return ObjectID(id)});
ctx.body  =  await ctx.mongo.db('Mojito').collection('item').find({ "_id":{$in:convertIdToObjectId}}).toArray()
});

router.post("/item",async function(ctx){
    let group = ctx.request.body
    try {
ctx.body = await ctx.mongo.db('Mojito').collection('group_item').insertMany( sanitize(ctx.request.body) );
}
catch (e) {
console.log(e);
 }

})
router.post('/item/:id', async function (ctx) {
  let groupId=ctx.params.id;
    let group = ctx.request.body
        let insertItem = await ctx.mongo
        .db('Mojito')
        .collection('item')
        .insertMany(ctx.request.body.insertItem)
 
        let selectGroupItem =  await ctx.mongo.db('Mojito').collection('group_item').find({"_id":sanitize(ObjectID(groupId))},{_id:0,item_ids:1}).toArray();
        let itemId = selectGroupItem[0].item_ids;
        let allItem = itemId.concat(insertItem.insertedIds.map(x=>x.toString()))
         await ctx.mongo.db('Mojito').collection('group_item').update(
          { _id: sanitize(ObjectID(groupId)) },
          {
            $set: {
              item_ids: sanitize(allItem)
            }
          }
        )   
    
        ctx.body =insertItem.insertedIds.toString()
// ctx.body=insertItem.insertedIds.toString()
        //  let convertIdToObjectId = allItem.map(function (id) {return ObjectID(id)});
        //  ctx.body  =  await ctx.mongo.db('Mojito').collection('item').find({ "_id":{$in:convertIdToObjectId}}).toArray()
  
        })
        router.post('/saveall/:id', async function (ctx) {
          let groupId=ctx.params.id;
                 let editItem =ctx.request.body.editItem;
                 if(editItem.length != 0){
                  for (i = 0; i < editItem.length; i++) { 
                   ctx.body = await ctx.mongo.db('Mojito').collection('item').update(
                      { _id: sanitize(ObjectID(editItem[i]._id)) },
                      {
                        $set: {
                          item_type : sanitize(editItem[i].item_type), 
                          payload : sanitize(editItem[i].payload), 
                          name : sanitize(editItem[i].name)
                        }
                      }
                    )
                  }
                 }
                })
router.post('/allitem/:id', async function (ctx) {
let groupId=ctx.params.id;
  let group = ctx.request.body
      let insertItem = await ctx.mongo
      .db('Mojito')
      .collection('item')
      .insertMany(ctx.request.body.insertItem)
      let selectGroupItem =  await ctx.mongo.db('Mojito').collection('group_item').find({"_id":sanitize(ObjectID(groupId))},{_id:0,item_ids:1}).toArray();
      let itemId = selectGroupItem[0].item_ids;
      let allItem = itemId.concat(insertItem.insertedIds.map(x=>x.toString()))
     let updateGroupItem = await ctx.mongo.db('Mojito').collection('group_item').update(
        { _id: sanitize(ObjectID(groupId)) },
        {
          $set: {
            item_ids: sanitize(allItem)
          }
        }
      )       
       let editItem =ctx.request.body.editItem;
       if(editItem.length != 0){
        for (i = 0; i < editItem.length; i++) { 
         ctx.body = await ctx.mongo.db('Mojito').collection('item').update(
            { _id: sanitize(ObjectID(editItem[i]._id)) },
            {
              $set: {
                item_type : sanitize(editItem[i].item_type), 
                payload : sanitize(editItem[i].payload), 
                name : sanitize(editItem[i].name)
              }
            }
          )
        }
       }
       let convertIdToObjectId = allItem.map(function (id) {return ObjectID(id)});
       ctx.body  =  await ctx.mongo.db('Mojito').collection('item').find({ "_id":{$in:convertIdToObjectId}}).toArray()

      })
router.delete("/item/:id",async function(ctx){
    let item = ctx.request.body
    await ctx.mongo.db('Mojito').collection('item').deleteOne( {"_id" : sanitize(ObjectID(item._id))} )
    let selectGroupItem =  await ctx.mongo.db('Mojito').collection('group_item').find({"_id":sanitize(ObjectID(ctx.params.id))},{_id:0,item_ids:1}).toArray();
    let itemId = selectGroupItem[0].item_ids;
    let filteredAry = itemId.filter(e => e !== item._id)
    let convertIdToObjectId = filteredAry.map(function (id) {return ObjectID(id)});
    ctx.body  =  await ctx.mongo.db('Mojito').collection('item').find({ "_id":{$in:convertIdToObjectId}}).toArray() 

})
router.post('/deleteitem/:id', async function (ctx) {
  let item = ctx.request.body
    await ctx.mongo.db('Mojito').collection('item').deleteOne( {"_id" : sanitize(ObjectID(item._id))} )
    let selectGroupItem =  await ctx.mongo.db('Mojito').collection('group_item').find({"_id":sanitize(ObjectID(ctx.params.id))},{_id:0,item_ids:1}).toArray();
    let itemId = selectGroupItem[0].item_ids;
    let filteredAry = itemId.filter(e => e !== item._id)
    let convertIdToObjectId = filteredAry.map(function (id) {return ObjectID(id)});
    ctx.body  = await ctx.mongo.db('Mojito').collection('group_item').update(
      { _id: sanitize(ObjectID(ctx.params.id)) },
      {
        $set: {
          item_ids: sanitize(filteredAry)
        }
      }
    )       
    // ctx.body  =  await ctx.mongo.db('Mojito').collection('item').find({ "_id":{$in:convertIdToObjectId}}).toArray() 

})

router.get("/collectionall",async function (ctx) {
  ctx.body = await ctx.mongo
    .db('Mojito')
    .collection('collection')
    .find({}, { name: 1 })
    .toArray()
  });

  router.get("/collection/:id", async function (ctx) {
ctx.body =await ctx.mongo.db('Mojito').collection('collection_groupitem').aggregate([
      { $match:{"_id" : ObjectID(ctx.params.id)}},
      { $lookup:
         {
           from: 'collection',
           localField: 'collection_ids',
           foreignField: '_id',
           as: 'collection',
    
         }
       }
      ,
    
       { $lookup:
        {
          from: 'group_item',
          localField: 'group_ids',
          foreignField: '_id',
          as: 'group'
        }
      }
      ,
    
      { $project : { 
        _id:1, 
        "collection._id":1,
        "collection.name":1,
        "group._id":1,
        "group.name" : 1,
      "group.object_type" :1, 
      "group.random" : 1, 
      "group.item_ids" : 1, 
    
    } } 
     
      ]).toArray()
    
    });

router.post("/collection",async function(ctx){
      let group = ctx.request.body
      try {
  ctx.body = await ctx.mongo.db('Mojito').collection('collection').insertMany( sanitize(ctx.request.body) );
  }
  catch (e) {
  console.log(e);
   }
  
  })

router.post("/collectionaddgroup/:id",async function(ctx){
    let group = ctx.request.body
    try {
ctx.body = await ctx.mongo.db('Mojito').collection('collection_groupitem').insertMany( sanitize(ctx.request.body) );
}
catch (e) {
console.log(e);
 }

})
app.use(cors())
app.use(bodyParser())
app.use(router.routes()).use(router.allowedMethods())
app.listen(3000, () => {
  console.log('listening on port 3000')
})
