import React, { Component } from 'react'
import { Card } from 'antd'

export default class Photo extends Component {
  render () {
    return (
      <div id="companyPhoto">
        <Card>
          <img alt="example"
            src="http://mojito.ai/img/local_language.png" />
        </Card>
      </div>
    )
  }
}
