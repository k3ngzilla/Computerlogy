import React, { Component } from 'react'
import { Tabs } from 'antd'
import Listitems from './Listitems'
const TabPane = Tabs.TabPane

export default class MainNav extends Component {
  constructor (props) {
    super(props)
    this.state = {
      mode: 'left'
    }
  }

  render () {
    const { mode } = this.state
    return (
      <div>
        <Tabs defaultActiveKey='1' tabPosition={mode} style={{ height: 220 }}>
          <TabPane tab='Group Item' key='1' ><Listitems /></TabPane>
          <TabPane tab='Collection' key='2'></TabPane>
        </Tabs>
      </div>
    )
  }
}
