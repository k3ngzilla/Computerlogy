// import React, { Component } from 'react'
// import { Row, Col, Layout, Input, Button, Icon } from 'antd'
// import DisplayEach from './DisplayEach'
// const { Header, Content } = Layout
// const { TextArea } = Input

// class Display extends Component {
//     constructor (props) {
//       super(props)
//       this.state = {
//           newComponent: false
//       }
//       this.addNewTextCard = this.addNewTextCard.bind(this)
//     }

//     addNewTextCard() {
//         this.setState({
//             newComponent: true
//         })
//     }

//   render () {
//     let { allInputText } = this.props
//     return (
//       <Col span={14} id='displayArea'>
//         <div id='displayAreaInner'>
//           <h2 id='headerTitle'>Welcome Message</h2>
//           <div className='cardTextWrapper'>

//             <Row>
//               <ul>
//                 {allInputText.map((eachText, index) => (
//                   <li key={index}>
//                     <DisplayEach eachCard={eachText} />
//                   </li>
//                 ))}
//                 {this.state.newComponent ? <DisplayEach /> : null }
//               </ul>
//             </Row>

//             <div id='saveAll'>
//               <Button type='primary'>Save All</Button>
//             </div>

//           </div>
//         </div>

//         <div id='addComponentWrapper'>
//           <h2>+ Add a card</h2>
//           <ul id='listAaddComponent'>
//             <li>
//               <Button onClick={() => this.addNewTextCard() }>
//                 <Icon type='file-text' />Text
//               </Button>
//             </li>
//           </ul>
//         </div>
//       </Col>
//     )
//   }
// }

// export default Display
