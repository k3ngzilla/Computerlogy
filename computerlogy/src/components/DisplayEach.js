import React, { Component } from 'react'
import { Col, Input, Icon } from 'antd'
// const { Header, Content } = Layout;
const { TextArea } = Input

class DisplayEach extends Component {
  constructor (props) {
    super(props)
    this.state = this.props
    
    console.log(this.props)
    this.fetchDelete = this.fetchDelete.bind(this)
  }
async getfunction(){
  this.props.getfunction()
}
  async fetchDelete(idgroup,iditem) {
  let result=  await fetch('http://localhost:3000/deleteitem/'+idgroup, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(
        {
          _id: iditem
        }
      )
    })
this.getfunction()
return result;
    
  }
  async fetchput(iditem,text) {
    let result=  await fetch('http://localhost:3000/item', {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(
          { _id:iditem,
         payload: {
            text: text
          }}

        )
      })
      
      this.getfunction()
      return result;
    }
  async fetchPostItem (text) {
    const result = await fetch('http://localhost:3000/item', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({insertItem:[
        {
          item_type: 'text',
          payload: {
            text: text
          },
          name: 'text_02'
        }
      ],editItem:[]
    }
      )
    })
    if (result.ok) {
      let data = await result.json()
      

      let listItem = this.state.eachCard.concat(data)
      this.setState({ eachCard: [...listItem] })
    
    }
  }
  async fetchPostGroupItem(insertedId) {
   
  }
  test(text) { console.log("delete"+text) }
  render () {
    let { eachCard, DataEdit,DataInsert,Groupitemid,getfunction } = this.state
    return (
      <Col span={8} className='cardTextInnerWrapper'>
        <ul id='cardTextControl'>
          <li><a onClick={() => this.fetchDelete(Groupitemid,eachCard._id)} ><Icon type='delete' /></a></li>
        </ul>
        <TextArea
          value={eachCard.payload.text}
          key = {eachCard._id}
          onChange={event => {
            this.setState({eachCard:{_id:eachCard._id,name:eachCard.name,payload:{text:event.target.value }}})
          }}
           onKeyPress={e => e.key === 'Enter' && this.fetchput(eachCard._id,eachCard.payload.text) }
          placeholder='Hi! ... I am robot. May I help you?'
          autosize={{ minRows: 2, maxRows: 6 }}
        />
      </Col>
    )
  }
}

export default DisplayEach
