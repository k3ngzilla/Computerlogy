import React, { Component } from 'react'
import './App.css'
import Photo from './components/Photo'
import DisplayEach from './components/DisplayEach'
import { Row, Col, Layout, Button,List, Icon, Tabs, Input } from 'antd'
const { Header, Content } = Layout
const TabPane = Tabs.TabPane

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      CompanyName: 'Mojito',
      inputText: '',
      newComponent: false,
      cards: [],
      mode: 'left',
      dataInsert:[],
      dataEdit:[],
      groupItem:[],
      groupid:'',
      groupname:'',
      showgroupname:''
    }
    // this.addNewTextCard = this.addNewTextCard.bind(this)
    this.addNewTextCard = this.addNewTextCard.bind(this)
    this.fetchGet = this.fetchGet.bind(this)
    this.fetchPost = this.fetchPost.bind(this)
    this.fetchGetAllGroupItem = this.fetchGetAllGroupItem.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.fetchPostAll = this.fetchPostAll.bind(this)
  }
  componentDidMount () {
 
    this.fetchGetAllGroupItem()
  }
  async fetchGetAllGroupItem(){
    this.setState({groupItem: this.state.groupItem=[]});
 
    const result = await fetch(
      
      'http://localhost:3000/groupitemall'
    )
    let data = await result.json()
    let d = data.map(each => {
      return each
    })
    this.setState({groupItem: this.state.groupItem.concat(d)});
   
    
  }
  async fetchGet (groupItemId,groupname) {
    this.setState({ groupid: this.state.groupid=''})
    this.setState({showgroupname: this.state.showgroupname=''});
    this.setState({cards:this.state.cards=[]})
   this.setState({ groupid: this.state.groupid=groupItemId})
   this.setState({showgroupname: this.state.showgroupname=groupname});
    const result = await fetch(
      
      'http://localhost:3000/item/'+groupItemId
    )
    let data = await result.json()
    let d = data.map(each => {
      return each
    })
    this.setState({ cards: this.state.cards.concat(d)});
    console.log(this.state.groupid)
  }
  async fetchPost() {
  
    const result = await fetch('http://localhost:3000/item/'+this.state.groupid, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        insertItem:[{
             "item_type": "text",
             "payload": {
                 "text": ""
             },
             "name": "text_"+this.state.cards.length
        }]
      })
    })
    if (result.ok) {
      return result
    }
  }

  async fetchPostAll() {
  
    const result = await fetch('http://localhost:3000/saveall/'+this.state.groupid, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        editItem:this.state.cards
      })
    })
    if (result.ok) {
      return result
    }
  }
  async fetchPostGroup() {
    const result = await fetch('http://localhost:3000/groupitem', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "object_type":"",
        "name": this.state.groupname
      })
    })
    if (result.ok) {
      this.setState({ groupname: this.state.groupname=''})
      this.fetchGetAllGroupItem()
      return result
    }

  }
 async addNewTextCard () {

    let insertid =  await  this.fetchPost();
    this.setState({
     cards: this.state.cards.concat([{
      "item_type": "text",
      "payload": {
          "text": this.state.inputText
      },
      "name": "text_"+this.state.cards.length
     }])
 
    })
    this.fetchGet(this.state.groupid)
  }
  async addNewImageCard () {

    let insertid =  await  this.fetchPost();
    this.setState({
     cards: this.state.cards.concat([{
      "item_type": "image",
      "payload": {
          "url": this.state.inputText
      },
      "name": "image_"+this.state.cards.length
     }])
 
    })
    this.fetchGet(this.state.groupid)
  }
  handleChange (e) {
    this.setState({ cards: [...this.state.cards, e.target.value] })
  }
  // addNewTextCard (newText) {
  //   this.setState({
  //     cards: this.state.cards.concat([newText])
  //   })
  // }
  test(){
    console.log(this.state.groupname)
    this.setState({ groupname: this.state.groupname=''})
  }
  render () {
    let { cards, inputText ,dataEdit,dataInsert} = this.state
    const { mode } = this.state   
    return (
   
      <div className='App'>
        <Layout>
          <Header id='mainHeader'>
            <h1 id='companyTitle'>{this.state.CompanyName}</h1>
          </Header>
          <Content>
            <Row>
              <Col id='leftSection'>
                <Photo />
                <div id="innerSectionWrapper">
                  <Tabs defaultActiveKey='1' tabPosition={mode}>
                    <TabPane tab='Group Item' key='1' >
                      <div class="groupList">

                        {/* <ul>
                          <li><button className='btnlist'>Ex Type Button</button></li>
                          <li><button className='btnlist'>Ex Type Text</button></li>
                        </ul> */}
                <List
                bordered
                dataSource={this.state.groupItem}
                renderItem={(item,index) => (    
                 <List.Item>
                   <button className='btnlist' onClick={() => this.fetchGet(item._id,item.name)   }>{item.name} </button>
                 </List.Item> 
              )}/>
                        <h4 className='alignLeft'>+ ADD NEW GROUP ITEM</h4>
                        <Input 
                        onChange={ (e) => {this.setState({ groupname: this.state.groupname= e.target.value })}}
                         value={this.state.groupname}
                         onKeyPress={e => e.key === 'Enter' && this.fetchPostGroup() } />
                      </div>

                      <div class="groupDisplayItems">
                        <Col id='displayArea'>
                          <div id='displayAreaInner'>
                            <h2 id='headerTitle'  >{this.state.showgroupname}</h2>
                            <div className='cardTextWrapper'>
                              <Row>
                                <ul>
                                  {cards.map((eachText, index) => (
                          
                                    <li key={index}>
                                  
                                      <DisplayEach eachCard={ eachText} getfunction={()=>{this.fetchGet(this.state.groupid)}} DataEdit={dataEdit} Groupitemid={this.state.groupid} DataInsert={dataInsert} />
                                    </li>
                                  ))}
                                  {this.state.newComponent
                                    ? <DisplayEach eachCard={inputText} />
                                    : null}
                                </ul>
                              </Row>

                              {/* <div id='saveAll'>
                                <Button onClick={() => this.fetchPostAll()} type='primary'>Save All</Button>
                              </div> */}

                            </div>
                          </div>

                          <div id='addComponentWrapper'>
                            <h2>+ Add a card</h2>
                            <ul id='listAaddComponent'>
                              <li>
                                <Button onClick={() => this.addNewTextCard()}>
                                  <Icon type='file-text' />Text
                                </Button>
                                <Button onClick={() =>  this.addNewImageCard ()}>
                                  <Icon type="picture" />Image
                                  </Button>
                              </li>

                            </ul>
                          </div>
                        </Col>
                      </div>
                    </TabPane>
                    <TabPane tab='Collection' key='2'>
                      <div class="groupList">
                        <ul>
                          <li><button className='btnlist'>Ex Type Button</button></li>
                          <li><button className='btnlist'>Ex Type Text</button></li>
                        </ul>
                        <h4 className='alignLeft'>+ ADD NEW COLLECTION</h4>
                        <Input />
                      </div>
                    </TabPane>
                  </Tabs>
                </div>
              </Col>
            </Row>
          </Content>
        </Layout>
      </div>
    )
  }
}

export default App
