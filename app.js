const Koa = require('koa');
const Router = require('koa-router');
const app = new Koa();
const router = new Router();

router.get('/', (ctx, next) => {
    const firstname = "Nattapol , phongphichan , Chinapat";
    ctx.body = firstname;
})

.get('/first_page', (ctx, next) => {
    const lastname = "Bumsuntiea , wicchaisan , Sirapatdomrongkul";
    ctx.body = firstname;

})

.get('/second_page', (ctx, next) => {
    const gender = "male";
    ctx.body = gender;
})
.get('/third_page', (ctx, next) => {
    const skill = `
        - HTML<br>
        - CSS3<br>
        - JavaScript<br>
        - React <br>
        - NodeJS<br>
        - MySQL<br>
        - testtest<br>
        - Saxophone<br>
    `;
    ctx.body = skill;
})
.get('/fourth_page', (ctx, next) => {
    const MyProjectDescription = "มีประโยชน์ในการช่วยเพิ่มการขายให้ร้านค้าขายของได้ง่ายขึ้น";
    ctx.body = MyProjectDescription;
})
.get('/fifth_page', (ctx, next) => {
    const MyProjectDescription = "ช่วยให้ลูกค้า และร้านค้าติดต่อกันได้ง่ายขึ้น";
    ctx.body = MyProjectDescription;
});

app
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(3000);
